//Constants
const Discord = require('discord.js');
const client = new Discord.Client();
const TOKEN = 'Mzk0MTY2NDcxNTY5MzA5Njk3.DTA0XA.cYaHGnh-dQ8-kKs9jsyE2HnESe0';
const PREFIX = '!'
const fs = require("fs");
const ytdl = require("ytdl-core");
const request = require("request");

//Variables
var bot = new Discord.Client();

//Startup
bot.on('ready', function() {
    console.log('Successfully logged in as Aeroscopic#1211');
});
//Commands
bot.on('message', function(message) {
    if (message.author.equals(bot.user)) return;

    if (!message.content.startsWith(PREFIX)) return;

    var args = message.content.substring(PREFIX.length).split(' ');

    switch (args[0].toLowerCase()) {
        case 'help':
            message.channel.send('Future help embed in works!')
            break;

        case 'about':
            var about = new Discord.RichEmbed()
                .addField('**Aeroscopic**', 'An All-in-One Discord bot by **Aarnav Tale**')
                .setColor(0x3a71c1)
                .setFooter('Aeroscopic © Aarnav Tale 2018')
                .setThumbnail('https://cdn.discordapp.com/attachments/390274222187872268/398850787297460224/Aeroscopic_Logo.png')
            message.channel.sendEmbed(about);
            break;

        default:
            message.channel.sendMessage('Unknown command, please type !help')
    }
})
//Login For BOT
bot.login(TOKEN);

//Express
const express = require('express');
const app = express();

// set the port of our application
// process.env.PORT lets the port be set by Heroku
const port = process.env.PORT || 5000;

// set the view engine to ejs
app.set('view engine', 'ejs');

// make express look in the `public` directory for assets (css/js/img)
app.use(express.static(__dirname + '/public'));

// set the home page route
app.get('/', (request, response) => {
    // ejs render automatically looks in the views folder
    response.render('index');
});

app.listen(port, () => {
    // will echo 'Our app is running on http://localhost:5000 when run locally'
    console.log('Our app is running on http://localhost:' + port);
});
 // pings server every 15 minutes to prevent dynos from sleeping
 setInterval(() => {
    http.get('http://aeroscopic.herokuapp.com');
  }, 900000);